import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import math

def rand_number():
	return np.random.randint(21, size = 1) - 10

def init_param():
    kp = 3
    ko = 5
    d0 = 20
    return kp, ko, d0

# kp wpływa na siłę przyciągania przez punkt końcowy.
# Im większe kp tym większe wartości związane z odległością od punktu końcowego.
#
# koi wpływa na siłe odpychania przez przeszkody.
# Im większe koi tym mocniej przeszkody "odpychają od siebie".
#
# d0 wpływa na siłe odpychania przez przeszkody.
# Im większy parametr d0 tym większy jest promień odpychania przez przeszkody oraz wartość odpychania zmienia się mniej gwałtownie niż w przypadku małego parametru d0.

kp, koi, d0 = init_param()
start_point = (-10, rand_number())
finish_point = (10, rand_number())
robot = start_point
delta = 0.1

obstacles = [(rand_number(), rand_number()), (rand_number(), rand_number()), (rand_number(), rand_number()), (rand_number(), rand_number())]

def Fp(qr, qk, kp):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qk[0]
    sum_vec[1] = qr[1] - qk[1]
    return kp * np.linalg.norm(sum_vec)


def Foi(qr, qoi, koi, d0):
    sum_vec = [0, 0]
    sum_vec[0] = qr[0] - qoi[0]
    sum_vec[1] = qr[1] - qoi[1]
    if np.linalg.norm(sum_vec) <= d0:
        return -1 * koi * (1 / np.linalg.norm(sum_vec) - 1 / d0) * (1 / (np.linalg.norm(sum_vec) * np.linalg.norm(sum_vec)))
    else:
        return 0

@np.vectorize
def calcZ(qrx, qry):
	qr = [qrx, qry]
	Zmatrix = [0, 0, 0, 0, 0]
	Zmatrix[0] = Fp(qr, finish_point, kp)
	Zmatrix[1] = Foi(qr, obstacles[0], koi, d0)
	Zmatrix[2] = Foi(qr, obstacles[1], koi, d0)
	Zmatrix[3] = Foi(qr, obstacles[2], koi, d0)
	Zmatrix[4] = Foi(qr, obstacles[3], koi, d0)
	return Zmatrix[0]-Zmatrix[1]-Zmatrix[2]-Zmatrix[3]-Zmatrix[4]

def calcAngle(vec):
    x = vec[0]
    y = vec[1]
    cosAngle = float(x)/(math.sqrt(float(x**2)+float(y**2)))
    return math.acos(cosAngle)

def plot(start_point, finish_point, robot, obstacles, Z):
    x_size = 10
    y_size = 10
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    ax.set_title('Metoda potencjałów')
    plt.imshow(Z, cmap=cm.RdYlGn,
               origin='lower', extent=[-x_size, x_size, -y_size, y_size],
               vmax=120, vmin=0)

    plt.plot(start_point[0], start_point[1], "or", color='blue')
    plt.plot(finish_point[0], finish_point[1], "or", color='blue')
    plt.plot(robot[0], robot[1], "or", color='red')

    for obstacle in obstacles:
        plt.plot(obstacle[0], obstacle[1], "or", color='black')

    plt.colorbar(orientation='vertical')

    plt.grid(True)
    plt.show()


x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = calcZ(X, Y)
print(Z)
#print(calcAngle(Z))

plot(start_point, finish_point, robot, obstacles, Z)
