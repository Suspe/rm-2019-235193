import numpy as np
from math import sqrt, atan2, cos, sin
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from drawnow import drawnow

def init_param():
    kp=5
    ko=5
    d0=10
    dr=5
    return kp, ko, d0, dr

def rand_number():
    return (np.random.randint(21, size=1)-10)[0]

def Up(qr, qk, kp):
    sum_vec=[0, 0]
    sum_vec[0]=qr[0]-qk[0]
    sum_vec[1]=qr[1]-qk[1]
    return 0.5*kp*np.linalg.norm(sum_vec)*np.linalg.norm(sum_vec)

def Voi(qr, qoi, koi, d0):
    if qr[0]==qoi[0] and qr[1]==qoi[1]:
        return 0
    sum_vec=[0, 0]
    sum_vec[0]=qr[0]-qoi[0]
    sum_vec[1]=qr[1]-qoi[1]
    if np.linalg.norm(sum_vec)<=d0:
        return 0.5*koi*(1/np.linalg.norm(sum_vec)-1/d0)*(1/np.linalg.norm(sum_vec)-1/d0)
    else:
        return 0

def Uw(qr, qk, obstacles, kp, koi, d0):
    Vsum=0
    for obstacle in obstacles:
        Vsum+=Voi(qr, obstacle, koi, d0)
    return Up(qr, qk, kp) + Vsum

def Fp(qr, qk, kp):
    sum_vec=[0, 0]
    sum_vec[0]=qr[0]-qk[0]
    sum_vec[1]=qr[1]-qk[1]
    return kp*np.linalg.norm(sum_vec)

def Foi(qr, qoi, koi, d0):
    if qr==qoi:
        return 0
    sum_vec=[0, 0]
    sum_vec[0]=qr[0]-qoi[0]
    sum_vec[1]=qr[1]-qoi[1]
    if np.linalg.norm(sum_vec) <= d0:
        return -1*koi*(1/np.linalg.norm(sum_vec)-1/d0)*(1/(np.linalg.norm(sum_vec)*np.linalg.norm(sum_vec)))
    else:
        return 0

def plot(start_point, finish_point, robot, obstacles, Z, robot_path):
    x_size = 10
    y_size = 10
    plt.imshow(Z, cmap=cm.RdYlGn,
               origin='lower', extent=[-x_size, x_size, -y_size, y_size],
               vmax=9, vmin=0)
    plt.plot(start_point[0], start_point[1], "or", color='blue')
    plt.plot(finish_point[0], finish_point[1], "or", color='blue')
    plt.plot(robot[0], robot[1], "or", color='red')
    for point in robot_path:
        plt.plot(point[0], point[1], "or", color='blue')
    for obstacle in obstacles:
        plt.plot(obstacle[0], obstacle[1], "or", color='black')
    plt.colorbar(orientation='vertical')
    plt.grid(True)

def calc_angle(pointA, pointB):
    vec=[pointB[0]-pointA[0], pointB[1]-pointA[1]]
    return atan2(vec[1], vec[0])

def calc_xy(vector):
    x=vector[1]*cos(vector[0])
    y=vector[1]*sin(vector[0])
    return [x, y]

def calc_out_angle(finish_vec, obstacle_vec):
    finish_vec=calc_xy(finish_vec)
    x=finish_vec[0]
    y=finish_vec[1]
    for obstacle in obstacle_vec:
        obstacle=calc_xy(obstacle)
        x=x+obstacle[0]
        y=y+obstacle[1]
    return calc_angle([0, 0], [x, y])

def move_robot(robot, next_point):
    if next_point==1:
        return [robot[0]+0.1, robot[1]]
    if next_point==2:
        return [robot[0]+0.1, robot[1]+0.1]
    if next_point==3:
        return [robot[0], robot[1]+0.1]
    if next_point==4:
        return [robot[0]-0.1, robot[1]+0.1]
    if next_point==5:
        return [robot[0]-0.1, robot[1]]
    if next_point==6:
        return [robot[0]-0.1, robot[1]-0.1]
    if next_point==7:
        return [robot[0], robot[1]-0.1]
    if next_point==8:
        return [robot[0]+0.1, robot[1]-0.1]

fig=plt.figure(figsize=(10, 10))
ax=fig.add_subplot(111)
ax.set_title('Metoda potencjalow')

kp, koi, d0, dr=init_param()
start_point=(-10, rand_number())
finish_point=(10, rand_number())
robot=start_point
delta=0.1
obstacles=[(rand_number(), rand_number()), (rand_number(), rand_number()), (rand_number(), rand_number()), (rand_number(), rand_number())]

@np.vectorize
def calcZ(qrx, qry):
	qr=[qrx, qry]
	Zmatrix=[0, 0, 0, 0, 0]
	Zmatrix[0]=Fp(qr, finish_point, kp)
	Zmatrix[1]=Foi(qr, obstacles[0], koi, d0)
	Zmatrix[2]=Foi(qr, obstacles[1], koi, d0)
	Zmatrix[3]=Foi(qr, obstacles[2], koi, d0)
	Zmatrix[4]=Foi(qr, obstacles[3], koi, d0)
	return Zmatrix[0]-Zmatrix[1]-Zmatrix[2]-Zmatrix[3]-Zmatrix[4]

x=y=np.arange(-10.0, 10.0, delta)
X,Y=np.meshgrid(x, y)

@np.vectorize
def find_next(qrx, qry, dr):
	qr=[qrx, qry]
	finish_vec=[calc_angle(qr, finish_point), Fp(qr, finish_point, kp)]
	obstacle_vec=[]
	for obstacle in obstacles:
		if sqrt((robot[0]-obstacle[0])**2+(robot[1]-obstacle[1])**2)<dr:
			obstacle_vec.append([calc_angle(qr, obstacle), Foi(qr, obstacle, koi, d0)])
	out_angle=calc_out_angle(finish_vec, obstacle_vec)
	out_angle=out_angle+np.pi
	if out_angle<=np.pi/8:
		return 5.0
	elif out_angle<=3*np.pi/8:
		return 6.0
	elif out_angle<=5*np.pi/8:
		return 7.0
	elif out_angle<=7*np.pi/8:
		return 8.0
	elif out_angle<=9*np.pi/8:
		return 1.0
	elif out_angle<=11*np.pi/8:
		return 2.0
	elif out_angle<=13*np.pi/8:
		return 3.0
	elif out_angle<=15*np.pi/8:
		return 4.0
	else:
		return 5.0

robot_path=[]
Z=find_next(X, Y, 999)

def update():
	plot(start_point, finish_point, robot, obstacles, Z, robot_path)

while robot[0]!=finish_point[0] or robot[1]!=finish_point[1]:
	robot_path.append(robot)
	robot=move_robot(robot, find_next(robot[0], robot[1], dr))
	drawnow(update)
	if abs(robot[0]-finish_point[0])<0.2 and abs(robot[1]-finish_point[1])<0.2:
		robot=finish_point
		robot_path.append(robot)
		break
drawnow(update)
plt.show()

